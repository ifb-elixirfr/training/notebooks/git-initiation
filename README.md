# A tutorial: git initiation with a Jupyter Notebook

This tutorial aims to introduce the basic of git and in parallel the Jupyter Notebooks.

⏱️ Duration: ~1h

## 📑 Outlines:

- Prerequisites
- Hands-on setup
- Configure git (config)
- First commit locally
- Push on a forge
- Using branches
- Other basic commands
- Contribute to an open-source project

## Usage

1. Just follow the first **"Hands-on setup"**
2. Enjoy your new Notebook!